const httpStatus = require("http-status");
const User = require("../models/user.model");
const News = require("../models/news.model");
const UserNews = require("../models/userNews.model");
const APIError = require("../utils/APIError");

exports.load = async (req, res, next) => {
  try {
    const news = await News.findById(req.params.newsId);

    if (news === null) {
      throw new APIError({
        status: httpStatus.FORBIDDEN,
        message: "newsId invalid ",
      });
    }

    req.news = news;
    return next();
  } catch (error) {
    return next(error);
  }
};

exports.list = async (req, res, next) => {
  const { page = 1, perPage = 30 } = req.query;
  const user = await User.findById(req.user.id);
  const listNews = await News.aggregate([
    {
      $match: {
        project: { $in: [req.user.project, "All"] },
        status: "publish",
      },
    },
    { $addFields: { _userId: user._id } },
    {
      $lookup: {
        from: "usernews",
        localField: "_id",
        foreignField: "news",
        as: "usernews",
      },
    },
    {
      $project: {
        titleEN: 1,
        titleTH: 1,
        detailTH: 1,
        detailEN: 1,
        startDate: 1,
        stopDate: 1,
        createdAt: 1,
        updatedAt: 1,
        project: 1,
        usernews: {
          $filter: {
            input: "$usernews",
            as: "usernews",
            cond: { $eq: ["$$usernews.user", "$_userId"] },
          },
        },
      },
    },
    {
      $addFields: {
        hasRead: {
          $cond: {
            if: { $gt: [{ $size: "$usernews" }, 0] },
            then: true,
            else: false,
          },
        },
      },
    },
    { $unset: ["usernews"] },
    { $skip: perPage * (page - 1) },
    { $limit: perPage },
    { $sort: { createdAt: -1 } },
  ]);

  res.json(listNews);
};

exports.create = async (req, res, next) => {
  const news = new News(req.body);
  const savedNews = await news.save();
  res.status(httpStatus.CREATED);
  res.json(savedNews);
};

exports.delete = async (req, res, next) => {
  const news = await News.findById(req.params.newsId);
  news.status = "remove";
  await news.save();
  res.json(news);
};

exports.readed = async (req, res, next) => {
  try {
    if (
      req.news.project !== req.user.project &&
      req.news.project.toUpperCase() !== "ALL" &&
      req.user.role === "user"
    ) {
      throw new APIError({
        status: httpStatus.FORBIDDEN,
        message: "Forbidden",
      });
    }

    const userNewsExist = await UserNews.findOne({
      user: req.user.id,
      news: req.news.id,
    });

    if (userNewsExist !== null) {
      userNewsExist.updatedAt = new Date();
      await userNewsExist.save();
      res.status(httpStatus.CREATED);
      return res.json();
    }

    const newUserNews = new UserNews({
      user: req.user.id,
      news: req.params.newsId,
    });
    await newUserNews.save();
    res.status(httpStatus.CREATED);
    return res.json();
  } catch (error) {
    return next(error);
  }
};
