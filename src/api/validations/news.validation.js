const Joi = require("joi");

module.exports = {
  getNews: {
    params: {
      newsId: Joi.string()
        .regex(/^[a-fA-F0-9]{24}$/)
        .required(),
    },
  },
  listNews: {
    query: {
      page: Joi.number().min(1),
      perPage: Joi.number().min(1).max(100),
      search: Joi.string(),
    },
  },
  createNews: {
    body: {
      titleTH: Joi.string().required(),
      titleEN: Joi.string().required(),
      detailTH: Joi.string().required(),
      detailEN: Joi.string().required(),
      project: Joi.string().required(),
      startDate: Joi.date(),
      stopDate: Joi.date(),
    },
  },
  updateNew: {
    params: {
      newsId: Joi.string()
        .regex(/^[a-fA-F0-9]{24}$/)
        .required(),
    },
    body: {
      titleTH: Joi.string().required(),
      titleEN: Joi.string().required(),
      detailTH: Joi.string().required(),
      detailEN: Joi.string().required(),
      project: Joi.string().required(),
      startDate: Joi.date(),
      stopDate: Joi.date(),
    },
  },
  deleteNews: {
    params: {
      newsId: Joi.string()
        .regex(/^[a-fA-F0-9]{24}$/)
        .required(),
    },
  },
  readed: {
    params: {
      newsId: Joi.string()
        .regex(/^[a-fA-F0-9]{24}$/)
        .required(),
    },
  },
};
