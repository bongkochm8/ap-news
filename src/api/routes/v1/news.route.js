const express = require("express");
const validate = require("express-validation");
const controller = require("../../controllers/news.controller");
const {
  listNews,
  createNews,
  deleteNews,
  readed,
} = require("../../validations/news.validation");
const { authorize, ADMIN, LOGGED_USER } = require("../../middlewares/auth");

const router = express.Router();

router.param("newsId", controller.load);

router
  .route("/")
  .get(authorize(LOGGED_USER), validate(listNews), controller.list)
  .post(authorize(ADMIN), validate(createNews), controller.create);

router
  .route("/:newsId")
  .delete(authorize(ADMIN), validate(deleteNews), controller.delete);

router
  .route("/:newsId/readed")
  .post(authorize(LOGGED_USER), validate(readed), controller.readed);

module.exports = router;
