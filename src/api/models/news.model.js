const mongoose = require("mongoose");

const status = ["pending", "publish", "remove"];

const newsSchema = new mongoose.Schema(
  {
    titleEN: {
      type: String,
      required: true,
      index: true,
    },
    titleTH: {
      type: String,
      required: true,
      index: true,
    },
    detailEN: {
      type: String,
      required: true,
      index: true,
    },
    detailTH: {
      type: String,
      required: true,
      index: true,
    },
    project: {
      type: String,
      required: true,
    },
    startDate: {
      type: Date,
    },
    stopDate: {
      type: Date,
    },
    status: {
      type: String,
      enum: status,
      default: "publish",
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("News", newsSchema);
