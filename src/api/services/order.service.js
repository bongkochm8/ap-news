const axios = require("axios");
const config = require("../../config/vars");
const APIError = require("../utils/APIError");
const querystring = require("querystring");

exports.orderHistory = async (authorization, query) => {
  try {
    console.log("query: ", query);

    // const axiosConfig = {
    //   headers: { Authorization: `${authorization}` },
    // };
    const url = `${config.localUri.order}/v1/order`;
    axios.defaults.headers.common["Authorization"] = authorization;

    const resp = await axios.get(url, { params: query });

    return resp.data;
    // return null;
  } catch (error) {
    console.log("get orderHistory error: ", error);
    return null;
  }
};
